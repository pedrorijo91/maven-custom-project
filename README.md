maven-custom-project
====================

From http://books.sonatype.com/mvnex-book/reference/customizing.html

To run:
    mvn exec:java 

Running with argument (look for weather in another zip, for instance "70112"):

    mvn exec:java -Dexec.args="70112"
